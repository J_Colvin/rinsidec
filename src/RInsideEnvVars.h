    const char *R_VARS[] = {
        "R_ARCH","",
        "R_BROWSER","false",
        "R_BZIPCMD","/bin/bzip2",
        "R_CMD","/usr/lib/R/bin/Rcmd",
        "R_DEFAULT_PACKAGES","",
        "R_DOC_DIR","/usr/share/R/doc",
        "R_ENVIRON","",
        "R_ENVIRON_USER","",
        "R_GZIPCMD","/bin/gzip -n",
        "R_HOME","/usr/lib/R",
        "R_INCLUDE_DIR","/usr/share/R/include",
        "R_INSTALL_PKG","RInsideC",
        "R_LIBS_SITE","/usr/local/lib/R/site-library:/usr/lib/R/site-library:/usr/lib/R/library",
        "R_LIBS_USER","~/R/x86_64-pc-linux-gnu-library/3.3",
        "R_OSTYPE","unix",
        "R_PACKAGE_NAME","RInsideC",
        "R_PAPERSIZE","letter",
        "R_PAPERSIZE_USER","letter",
        "R_PDFVIEWER","false",
        "R_PLATFORM","x86_64-pc-linux-gnu",
        "R_PRINTCMD","/usr/bin/lpr",
        "R_PROFILE","",
        "R_PROFILE_USER","/tmp/RtmpqXUtnZ/Rprofile-devtools",
        "R_RD4PDF","times,inconsolata,hyper",
        "R_SHARE_DIR","/usr/share/R/share",
        "R_SYSTEM_ABI","linux,gcc,gxx,gfortran,?",
        "R_TESTS","",
        "R_TEXI2DVICMD","/usr/bin/texi2dvi",
        "R_UNZIPCMD","/usr/bin/unzip",
        "R_VERSION","3.3.1",
        "R_ZIPCMD","/usr/bin/zip",
        NULL
    };
